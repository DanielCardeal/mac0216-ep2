#!/bin/bash
# AO PREENCHER(MOS) ESSE CABEÇALHO COM O(S) MEU(NOSSOS) NOME(S) E
# O(S) MEU(NOSSOS) NÚMERO(S) USP, DECLARO(AMOS) QUE SOU(MOS) O(S)
# ÚNICO(S) AUTOR(ES) E RESPONSÁVEL(IS) POR ESSE PROGRAMA. TODAS AS
# PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM DESENVOLVIDAS
# E IMPLEMENTADAS POR MIM(NÓS) SEGUINDO AS INSTRUÇÕES DESSE EP E QUE
# PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA OU PLÁGIO. DECLARO
# TAMBÉM QUE SOU(MOS) RESPONSÁVEL(IS) POR TODAS AS CÓPIAS DESSE
# PROGRAMA E QUE EU(NÓS) NÃO DISTRIBUÍ(MOS) OU FACILITEI(AMOS) A SUA
# DISTRIBUIÇÃO. ESTOU(AMOS) CIENTE(S) QUE OS CASOS DE PLÁGIO E
# DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
# DIVULGADOS NA PÁGINA DA DISCIPLINA. ENTENDO(EMOS) QUE EPS SEM
# ASSINATURA NÃO SERÃO CORRIGIDOS E, AINDA ASSIM, PODERÃO SER PUNIDOS
# POR DESONESTIDADE ACADÊMICA.
#
# Nome(s) : Daniel Pessoa Cardeal, Luis Davi Oliveira de Almeida Campos
# NUSP(s) : 10693170, 11849460
#
# Referências: Com exceção das rotinas fornecidas no enunciado e em
# sala de aula, caso você(s) tenha(m) utilizado alguma referência,
# liste(m) abaixo para que o programa não seja considerado plágio ou
# irregular.

# Diretório que representa um servidor
SERVER_DIR="/tmp/terminalchat-ser"
USERS_FILE="$SERVER_DIR/users"

# Começa uma sessão interativa como admin do servidor.
server_run() {
    # Inicialização do servidor
    if [ ! -d $SERVER_DIR ];then
        mkdir $SERVER_DIR
        > $USERS_FILE
    fi
    RUNNING=1
    SERVER_START=$(date +%s)

    # Garante que o servidor fechará corretamente caso ocorra
    # interrupção.
    trap 'server_end; echo ;exit 1' 2 15

    # Sessão do admin
    while [ $RUNNING -eq 1 ]; do
        echo -n "servidor> "
        read INPUT
        case $INPUT in
            list)
                cat $USERS_FILE | awk -F, '{print $1}'
                ;;

            time)
                echo $(date +%s) - $SERVER_START | bc
                ;;

            reset)
                > $USERS_FILE
                ;;

            quit)
                server_end
                RUNNING=0
                ;;

            *)
                echo ERRO
                ;;
        esac
    done
}

# Fecha o servidor atual.
server_end() {
    rm $SERVER_DIR -r
}

# Começa uma sessão interativa como cliente do chat
cliente_run() {
    if [ ! -d $SERVER_DIR ]; then
        # Servidor não iniciado
        echo ERRO
        exit 1
    fi

    RUNNING=1
    LOGGED=0

    # terminal onde está sendo rodado o cliente, com os caracteres '/'
    # escapados.
    TERMINAL=$(tty | sed 's/\//\\\//g')

    while [ $RUNNING -eq 1 ]; do
        echo -n "cliente> "
        read INPUT
        COMMAND=`echo $INPUT | grep -o '^[a-z]*'`
        case $COMMAND in
            create)
                USERNAME=$(echo $INPUT | awk '{print $2}')
                PASSWORD=$(echo $INPUT | awk '{print $3}')
                if [ "$USERNAME" = "" -o "$PASSWORD" = "" ]; then
                    # Poucos argumentos passados
                    echo ERRO
                    continue
                elif [ "$(grep -o "^$USERNAME" $USERS_FILE)" = "$USERNAME" ]; then
                    # Usuário já existe
                    echo ERRO
                    continue
                fi
                echo "$USERNAME,$PASSWORD" >> $USERS_FILE
                ;;

            passwd)
                USERNAME=$(echo $INPUT | awk '{print $2}')
                OLD=$(echo $INPUT | awk '{print $3}')
                NEW=$(echo $INPUT | awk '{print $4}')
                if [ "$USERNAME" = "" -o "$OLD" = "" -o "$NEW" = "" ]; then
                    # Poucos argumentos passados
                    echo ERRO
                    continue
                elif [ "$(grep "$USERNAME" $USERS_FILE | awk -F, '{print $2}')" != "$OLD" ]; then
                    # Falha de autenticação
                    echo ERRO
                    continue
                fi
                sed -i "s/^$USERNAME,$OLD/$USERNAME,$NEW/" $USERS_FILE
                ;;

            login)
                USERNAME=$(echo $INPUT | awk '{print $2}')
                PASSWORD=$(echo $INPUT | awk '{print $3}')
                if [ "$USERNAME" = "" -o "$PASSWORD" = "" ]; then
                    # Poucos argumentos passados
                    echo ERRO
                    continue
                elif [ "$(grep "$USERNAME" $USERS_FILE | awk -F, '{print $2}')" != "$PASSWORD" ]; then
                    # Falha de autenticação
                    echo ERRO
                    continue
                elif [ "$(grep "$USERNAME" $USERS_FILE | awk -F, '{print $3}')" != "" ]; then
                    # Usuário já logado em outro terminal
                    echo ERRO
                    continue
                elif [ $LOGGED -eq 1 ]; then
                    # Usuário já logado nesse terminal
                    echo ERRO
                    continue
                fi
                sed -i "s/^$USERNAME,$PASSWORD/$USERNAME,$PASSWORD,$TERMINAL/" $USERS_FILE
                LOGGED=1
                trap "sed -i 's/,$TERMINAL$//' $USERS_FILE; echo; exit 1" 2 15
                ;;

            logout)
                if [ $LOGGED -eq 0 ]; then
                    # Usuário não logado
                    echo ERRO
                    continue
                fi
                sed -i "s/,$TERMINAL$//" $USERS_FILE
                LOGGED=0
                ;;

            list)
                if [ $LOGGED -eq 0 ]; then
                    # Usuário não logado
                    echo ERRO
                    continue
                fi
                grep '/.*$' $USERS_FILE | awk -F, '{print $1}'
                ;;

            msg)
                if [ $LOGGED -eq 0 ]; then
                    # Usuário não logado
                    echo ERRO
                    continue
                fi

                RECEIVER=$(echo $INPUT | awk '{print $2}')
                if [ "$(grep "^$RECEIVER,.*,/.*$" $USERS_FILE)" = "" ]; then
                    # Destinatário não logado
                    echo ERRO
                    continue
                fi

                SENDER=$(grep "$TERMINAL$" $USERS_FILE | awk -F, '{print $1}')
                if [ "$SENDER" = "$RECEIVER" ]; then
                    # Tentativa de mandar mensagem para si mesmo
                    echo ERRO
                    continue
                fi
                RECEIVER_TERMINAL=$(grep "^$RECEIVER" $USERS_FILE | awk -F, '{print $3}')
                MESSAGE=$(echo $INPUT | sed "s/msg $RECEIVER /[Mensagem do $SENDER]: /")

                echo > $RECEIVER_TERMINAL
                echo $MESSAGE > $RECEIVER_TERMINAL
                echo -n "cliente> " > $RECEIVER_TERMINAL
                ;;

            quit)
                if [ $LOGGED -eq 1 ]; then
                    # Faz o logout do usuário logado
                    sed -i "s/,$TERMINAL$//" $USERS_FILE
                fi
                RUNNING=0
                ;;

            *)
                echo ERRO
                ;;
        esac
    done
}

# --- Main ---
case $1 in
    servidor) server_run;;
    cliente) cliente_run;;
esac

exit 0
